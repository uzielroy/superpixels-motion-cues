#include <assert.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include "cublas_v2.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#define N 307200
#define K 17
#define H 480
#define W 640

using namespace std;

void __global__ kernel_add_one(int* a, int length) {
    int gid = threadIdx.x + blockDim.x*blockIdx.x;

    while(gid < length) {
    	a[gid] += 1;
        gid += blockDim.x*gridDim.x;
    }
}

__global__
void Copyrik_k(float* x,float* result)
{
    int idx_x = blockIdx.x*K;
    int i;
    for(i=0;i<K;i++)
        result[idx_x+i]=x[idx_x+i];


}

__global__
void ChangePixels_k (int* numbers,int* ind,int mod,int* Lookup, float* r_ik,int* padded,float* rik_max) {
    int idx_num = blockIdx.x*9;
    int idx_ind=mod*int(N/4)*9+blockIdx.x*9;
    int i;
    int max_val,max_val_temp;
    int max_rik;
    int idx_pad;
    int idx_rik;
    if ((Lookup[numbers[idx_num + 1]] + Lookup[numbers[idx_num + 3]] + Lookup[numbers[idx_num + 4]] + Lookup[numbers[idx_num + 5]] + Lookup[numbers[idx_num + 7]])==5)
    {
        //current pixel
        idx_pad=ind[idx_ind+4];
        idx_rik=(int(idx_pad/(W+2))-1)*W+int(idx_pad%(W+2))-1;
        max_val=padded[idx_pad]; //current value
        max_rik=r_ik[idx_rik*K+max_val];

        //Up
        idx_pad=ind[idx_ind+1];
        max_val_temp=padded[idx_pad];
        if(r_ik[idx_rik*K+max_val_temp]>max_rik)
        {
            max_rik=r_ik[idx_rik*K+max_val_temp];
            max_val=max_val_temp;
        }

        //Left
        idx_pad=ind[idx_ind+3];
        max_val_temp=padded[idx_pad];
        if(r_ik[idx_rik*K+max_val_temp]>max_rik)
        {
            max_rik=r_ik[idx_rik*K+max_val_temp];
            max_val=max_val_temp;
        }

        //Right
        idx_pad=ind[idx_ind+5];
        max_val_temp=padded[idx_pad];
        if(r_ik[idx_rik*K+max_val_temp]>max_rik)
        {
            max_rik=r_ik[idx_rik*K+max_val_temp];
            max_val=max_val_temp;
        }

        //Down
        idx_pad=ind[idx_ind+7];
        max_val_temp=padded[idx_pad];
        if(r_ik[idx_rik*K+max_val_temp]>max_rik)
        {
            max_rik=r_ik[idx_rik*K+max_val_temp];
            max_val=max_val_temp;
        }
        for(i=0;i<K;i++)
        {
            if(i==max_val)
                rik_max[idx_rik*K+i]=1;
            else
                rik_max[idx_rik*K+i]=0;

        }
    }

}


__global__
void CreateMatrices_k(int* x,int* ind,int mod,int* result)
{
    int idx_ind=mod*int(N/4)*9+blockIdx.x*9;
    int idx_res=blockIdx.x*81;
    int idx_xj;
    int idx_xi;
    int idx_x;
    int i,j;

    for(i=0;i<9;i++)
    {
        for(j=0;j<9;j++)
        {
            idx_xi=ind[idx_ind+i];
            idx_xj=ind[idx_ind+j];
            if ((x[idx_xj]==x[idx_xi])||(x[idx_xj]==0))
                result[idx_res+i*9+j]=int(1);
            else
                result[idx_res+i*9+j]=int(0);
//            idx_x=ind[idx_ind+j];
//            result[idx_res+i*9+j]=int(x[idx_x]);
        }
    }
}

__global__
void CreateNumbers_k(int* x,int* result)
{
    int idx_x=blockIdx.x*81;
    int idx_res=blockIdx.x*9;
    int i;
    for(i=0;i<9;i++)
    {
        result[idx_res+i]=(x[idx_x+i*9+0])+(x[idx_x+i*9+1]<<1)+(x[idx_x+i*9+2]<<2)+(x[idx_x+i*9+3]<<3)+(x[idx_x+i*9+5]<<4)+(x[idx_x+i*9+6]<<5)+(x[idx_x+i*9+7]<<6)+(x[idx_x+i*9+8]<<7);
    }

}

__global__
void SoftMax_k(float* x,float* result)
{
    int idx_x =blockIdx.x*K;
    int i;
    float max_val=x[idx_x];
    float sum=0.0000000001;
    for(i=1;i<K;i++)
    {
        if(max_val<x[idx_x+i])
        {
            max_val=x[idx_x+i];
        }
    }
    for(i=0;i<K;i++)
    {
        result[idx_x+i]=exp(x[idx_x+i]-max_val);
    }

    for(i=0;i<K;i++)
    {
        sum+=result[idx_x+i];
    }
    for(i=0;i<K;i++)
    {
        result[idx_x+i]=result[idx_x+i]/sum;
    }

}



__global__
void Pi_k(float* x,float* result)
{
    int idx_x =blockIdx.x;
    result[idx_x]=x[idx_x]*(1)/N;
}


__global__
void AddDistances_k(float* x,float* y,float* pi, float* result )
{
    int idx_x =blockIdx.x*K;
    int i;
    for(i=0;i<K;i++)
    {
        result[idx_x+i]=(x[idx_x+i]+y[idx_x+i])+log(pi[i]);
    }
}




__global__
void DistanceCentersY_k(float* x,float* sigma,float* result ) //SIGMA is 3 elements
{
    int idx_x =blockIdx.x*3*K;
    int idx_res =blockIdx.x*K;
    int i;
    for(i=0;i<K;i++)
    {
        result[idx_res+i]=-(x[idx_x+0*K+i]*x[idx_x+0*K+i]*sigma[0]+x[idx_x+1*K+i]*sigma[1]*x[idx_x+1*K+i]+x[idx_x+2*K+i]*sigma[2]*x[idx_x+2*K+i]);
    }

}

__global__
void DistanceCentersX_k(float* x,float* sigma,float* result ) //SIGMA is 7 elements - 4 for x,y and 3 for L I B
{
    int idx_x =blockIdx.x*5*K;
    int idx_res =blockIdx.x*K;
    int i;
    for(i=0;i<K;i++)
    {
        result[idx_res+i]=-((x[idx_x+0+i*5]*sigma[0]+x[idx_x+1+i*5]*sigma[2])*(x[idx_x+0+i*5])+(x[idx_x+0+i*5]*sigma[1]+x[idx_x+1+i*5]*sigma[3])*(x[idx_x+1+i*5])+
        x[idx_x+2+i*5]*x[idx_x+2+i*5]*sigma[4]+x[idx_x+3+i*5]*x[idx_x+3+i*5]*sigma[5]+x[idx_x+4+i*5]*x[idx_x+4+i*5]*sigma[6]);
    }
}



__global__
void SubCentersY_k(float* x,float* c,int size,float* result)
{
    int idx_x =blockIdx.x*size;
    int idx_c = blockIdx.x*size*K;
    int idx_res =blockIdx.x*size*K;
    int i,j;

    for(i=0;i<size;i++)
    {
        for(j=0;j<K;j++)
        {
            result[idx_res+j+i*K]=x[idx_x+i]-c[idx_c+i*K+j];
        }
    }
}

__global__
void subCentersX_k(float* x,float* c,int size,float* result)
{
    int idx_x =blockIdx.x*size;
    int idx_res =blockIdx.x*size*K;
    int i,j;

    for(i=0;i<K;i++)
    {
        for(j=0;j<size;j++)
        {
            result[idx_res+i*size+j]=x[idx_x+j]-c[i*size+j];
        }
    }
}

__global__
void BmmMprimeT2_k(float* x,float* y,float* result) //multiplay K*2*2, by K *2  => K*2
{
    int idx_x = blockIdx.x*4;
    int idx_y = blockIdx.x*2;
    int idx_res =blockIdx.x*2;
    if (idx_x==0)
    {
        result[idx_res+0]=100;
        result[idx_res+1]=100;
    }

    else{
        result[idx_res+0]=(x[idx_x+0]*y[idx_y+0])+(x[idx_x+1]*y[idx_y+1]);
        result[idx_res+1]=(x[idx_x+2]*y[idx_y+0])+(x[idx_x+3]*y[idx_y+1]);

    }
}

__global__
void Det_k(float* x  , float* result)  //Det of K,2,2 => get K
{

    int idx_x = blockIdx.x*4;
    int idx_res = blockIdx.x;
    result[idx_res]=1.0/((x[idx_x+0]*x[idx_x+3])-(x[idx_x+1]*x[idx_x+2]));
}


__global__
void Inverse_k(float* x  ,float* det, float* result)  //Inverse K,2,2
{

    int idx_x = blockIdx.x*4;
    int idx_det = blockIdx.x;
    result[idx_x+0]=x[idx_x+3]*det[idx_det];
    result[idx_x+1]=-(x[idx_x+2]*det[idx_det]);
    result[idx_x+2]=-(x[idx_x+1]*det[idx_det]);
    result[idx_x+3]=x[idx_x+0]*det[idx_det];
}



__global__
void Div_k(float* x , float*y,  int size ,float* result)  //divide the second axis -(17 5)/ 17 -> 17 /5
{

    int idx_x = blockIdx.x*size;
    int idx_y = blockIdx.x;
    int i;
    if (idx_x==0)
    {
        for (i = 0; i < size; i++) {
            result[idx_x + i] = 1000;
        }
    }
    else {
        for (i = 0; i < size; i++) {
            result[idx_x + i] = x[idx_x + i] / y[idx_y];
        }
    }
}

__global__
void DivPi_k(float* x , int n ,float* result)  //divide X (K) , in 1 number
{

    int idx_x = blockIdx.x;
    result[idx_x]=x[idx_x]/n;
}




__global__
void Sum_k(float* x , int size ,float* result) // sum (N,k) - > K
{

    int idx = blockIdx.x*size;
    int idx_res=blockIdx.x;
    int i;
    result[idx_res]=0.0000000001;
    for (i=0;i<size;i++)
    {
        result[idx_res]+=x[idx+i];
    }
}


__global__
void Dot_k(float* x , int size ,float* result)
{

    int idx = blockIdx.x * size;
    int idx_res=blockIdx.x;
    int i;
    result[idx_res]=0;
    for (i=0;i<size;i++)
    {
        result[idx_res]+=(x[idx+i]*x[idx+i]);
    }
}


__global__
void BmmHTH_k(float* x ,float* result) //size 3*2  (( 2x3 * 3x2 = 2x2)
{

    int idx = blockIdx.x*6;
    int idx_res=blockIdx.x*4;
    result[idx_res+0]=(x[idx+0]*x[idx+0])+(x[idx+2]*x[idx+2])+(x[idx+4]*x[idx+4]);
    result[idx_res+1]=(x[idx+0]*x[idx+1])+(x[idx+2]*x[idx+3])+(x[idx+4]*x[idx+5]);
    result[idx_res+2]=result[idx_res+1];
    result[idx_res+3]=(x[idx+1]*x[idx+1])+(x[idx+3]*x[idx+3])+(x[idx+5]*x[idx+5]);
}

__global__
void BmmHTYT_k(float* x ,float* y,float* result) //size 2*1  (( 2x3 * 3x1 = 2x1)
{
    int idx_x = blockIdx.x*6;
    int idx_y = blockIdx.x*3;
    int idx_res=blockIdx.x*2;
    result[idx_res+0]=(x[idx_x+0]*y[idx_y+0])+(x[idx_x+2]*y[idx_y+1])+(x[idx_x+4]*y[idx_y+2]);
    result[idx_res+1]=(x[idx_x+1]*y[idx_y+0])+(x[idx_x+3]*y[idx_y+1])+(x[idx_x+5]*y[idx_y+2]);
}


__global__
void PaddedAMax_k(float* x,int size,int* result,float* rik_max)
{
    int idx_res = blockIdx.x;
    int idx_xh = (blockIdx.x / (W + 2));
    int idx_xw = (blockIdx.x % (W + 2));
    int idx_x = ((idx_xh-1)*W+(idx_xw-1))*size;
    int i;
    int amax = 0;
    int max = 0;
    if ((idx_res >= (W + 2)) && (idx_res <= ((W + 2) * (H + 1)))) {
        if (idx_res % (W + 2) == 0 || idx_res % (W + 2) == W + 1)
            result[idx_res] = 0;
        else {
            max = x[idx_x + 0];
            rik_max[idx_x+0]=0;
            for (i = 1; i < size; i++) {
                if (x[idx_x + i] > max) {
                    max = x[idx_x + i];
                    amax = i;
                }
                rik_max[idx_x+i]=0;
            }
            result[idx_res] = amax;
            rik_max[idx_x+int(amax)]=1;
        }
    } else {
        result[idx_res] = 0;
    }

}

void Dot(float* x,int size, float* result)
{
    Dot_k<<<N,1>>>(x,size,result);
}


void BmmHTH(float* x, float* result)
{
    BmmHTH_k<<<N,1>>>(x,result);
}

void BmmHTYT(float* x, float* y, float* result)
{
    BmmHTYT_k<<<N,1>>>(x,y,result);
}

void Mstep_SP(cublasHandle_t handle,float* x , float* r_ik_t,float* n_k, float * c1)
{
    float al =1.0f ;    // al =1
    float bet =1.0f ;    // bet =1
    Sum_k<<<K,1>>>(r_ik_t,N,n_k);
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, 5, K,N, &al, x, 5, r_ik_t, N,&bet,c1, 5 ) ;
    Div_k<<<K,1>>>(c1,n_k,5,c1);
}

void Mstep_SP_F(cublasHandle_t handle,float *r_ik_t,float *HH,float* YY,float* T1_temp,float* T1,float* T2,float* T3,float* det,float* m_prime_i,float* eta_prime,float* pi)
{
    float al =1.0f ;    // al =1
    float bet =1.0f ;    // bet =1
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, 2,K,N, &al, T1_temp, 2, r_ik_t, N,&bet,T1, 2 ) ; //T1
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, 4,K,N, &al, HH, 4, r_ik_t, N,&bet,T2, 4 ) ; //T2
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, 1,K,N, &al, YY, 1, r_ik_t, N,&bet,T3, 1 ) ; //T3

    // TODO: add m_0 to T2! (its small number, so its ok without it for now)
    // TODO: m_prime=0

    Det_k<<<K,1>>>(T2,det);
    Inverse_k<<<K,1>>>(T2,det,m_prime_i);
    BmmMprimeT2_k<<<K,1>>>(m_prime_i,T1,eta_prime);
    Sum_k<<<K,1>>>(r_ik_t,N,det); //det is the same size as nk
    DivPi_k<<<K,1>>>(det,N,pi);
}

void Estep_SP(cublasHandle_t handle,float* X,float* H_t,float* Y,float* r_ik,float* r_ik_t,float* pi,float* Nk,float* C1,float* C2,float* sigma1,float* sigma2,float* XC1,float* HC2,float* distances1,float* distances2,float* HC2_T)
{
    float al =1.0f ;    // al =1
    float bet =1.0f ;    // bet =1


    Sum_k<<<K,1>>>(r_ik_t,N,Nk);
    Pi_k<<<K,1>>>(Nk,pi);
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N,N*3,K,2, &al, H_t, N*3, C2, 2,&bet,HC2, N*3 ) ; // HC2_T K x N*3
    bet=0.0f;
    cublasSgeam( handle, CUBLAS_OP_T, CUBLAS_OP_N, K, N*3, &al, HC2, N*3, &bet, r_ik, K, HC2_T, K ); //transpose HC2
    SubCentersY_k<<<N,1>>>(Y,HC2_T,3,HC2); // HC2 will hold the result  // HC2 N*3 x K
    subCentersX_k<<<N,1>>>(X,C1,5,XC1); //XC1 N*K*5
    DistanceCentersX_k<<<N,1>>>(XC1,sigma1,distances1);
    DistanceCentersY_k<<<N,1>>>(HC2,sigma2,distances2);
    AddDistances_k<<<N,1>>>(distances1,distances2,pi,distances2); //Distances2 as output
    SoftMax_k<<<N,1>>>(distances2,r_ik);
}


void Connectivity(float* r_ik,float* rik_max,float* r_iknew,int* padded,int* matrices,int* numbers,int* ind,int mod,int* lookup)
{
    PaddedAMax_k<<<(H+2)*(W*2),1>>>(r_ik,K,padded,rik_max);
    CreateMatrices_k<<<int(N/4),1>>>(padded,ind,mod,matrices);
    CreateNumbers_k<<<int(N/4),1>>>(matrices,numbers);
    ChangePixels_k<<<int(N/4),1>>>(numbers,ind,mod,lookup,r_iknew,padded,rik_max);
    // Copyrik_k<<<N,1>>>(rik_max,r_ik);
}

