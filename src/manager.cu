/*
This is the central piece of code. This file implements a class
(interface in cuda_SP.hh) that takes data in on the cpu side, copies
it to the gpu, and exposes functions (increment and retreive) that let
you perform actions with the GPU

This class will get translated into python via swig
*/

#include <kernel.cu>
#include <manager.hh>
#include <assert.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include "cublas_v2.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
#define IDX2R(i ,j , ld ) ((( i )*( ld ))+( j ))
#define IDX2C(i ,j , ld ) ((( j )*( ld ))+( i ))
#define N 307200
#define K 17
#define dtype float
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

cuda_SP::cuda_SP (int* array_host_, int length_) {
  array_host = array_host_;
  length = length_;
  int size = length * sizeof(int);
  cudaError_t err = cudaMalloc((void**) &array_device, size);
  assert(err == 0);
  err = cudaMemcpy(array_device, array_host, size, cudaMemcpyHostToDevice);
  assert(err == 0);
}

void cuda_SP::increment() {
  kernel_add_one<<<64, 64>>>(array_device, length);
  cudaError_t err = cudaGetLastError();
  assert(err == 0);
}

void cuda_SP::retreive() {
  int size = length * sizeof(int);
  cudaMemcpy(array_host, array_device, size, cudaMemcpyDeviceToHost);
  cudaError_t err = cudaGetLastError();
  if(err != 0) { cout << err << endl; assert(0); }
}

void cuda_SP::retreive_to (int* array_host_, int length_) {
  assert(length == length_);
  int size = length * sizeof(int);
  cudaMemcpy(array_host_, array_device, size, cudaMemcpyDeviceToHost);
  cudaError_t err = cudaGetLastError();
  assert(err == 0);
}


/*void cuda_SP::Kmeans(float* array_host_ ,float* XData,float* H,float* Y){
    int i,j;
    float* X_m;
    float* H_m;
    float* Y_m;
    float* YY;
    X_m=( float *) malloc ( N * 5 * sizeof ( float ));
    H_m=( float *) malloc ( N * 3 * 2* sizeof ( float ));
    Y_m=( float *) malloc ( N * 3 * sizeof ( float ));
    YY=( float *) malloc ( N * 1 * sizeof ( float ));

    for ( j =0; j < 5 ; j ++)
    {
        for ( i =0; i < N ; i ++){
            X_m[ IDX2C(i ,j , N )]=( float ) XData[IDX2R(i,j,5)];
         }
    }

    for ( j =0; j < 3 ; j ++)
    {
        for ( i =0; i < N ; i ++){
            H_m[ IDX2C(i ,j , N )]=( float ) H[IDX2R(i,j,5)];
         }
    }

    for ( j =0; j < N ; j ++)
    {
        for ( i =0; i < 3 ; i ++){
            Y_m[IDX2C(i ,j   , 3 )]=( float ) Y[IDX2C(i,j,3)];
         }
    }

    std::cout << Y_m[0]<<"\n";
    std::cout << Y_m[1]<<"\n";
    std::cout << Y_m[2]<<"\n";
    std::cout << Y_m[3]<<"\n";
    std::cout << Y_m[4]<<"\n";
    std::cout << Y_m[5]<<"\n";

    std::cout << "End 6 elements \n";




    float * d_X ;    // d_a - a on the device
    float * d_H ;    // d_b - b on the device
    float * d_Y ;    // d_c - c on the device
    float * d_YY ;    // d_c - c on the device

    cudaError_t cudaStat ;
    // cudaMalloc status
    cublasStatus_t stat ;
    // CUBLAS functions status
    cublasHandle_t handle ;
    cudaStat = cudaMalloc (( void **)& d_X , N * 5 * sizeof (* X_m )); // device    // memory alloc for a
    cudaStat = cudaMalloc (( void **)& d_H , N * 6 * sizeof (* H_m )); // device    // memory alloc for b
    cudaStat = cudaMalloc (( void **)& d_Y , N * 3 * sizeof (* Y_m )); // device    // memory alloc for c
    cudaStat = cudaMalloc (( void **)& d_YY ,N * 1 * sizeof (* YY )); // device    // memory alloc for c
    stat = cublasCreate (& handle ); // initialize CUBLAS context
    // copy matrices from the host to the device
    stat = cublasSetMatrix (N ,5 , sizeof (* X_m ) ,X_m ,N , d_X , N ); // a -> d_a
    stat = cublasSetMatrix (N*3 ,2 , sizeof (* H_m ) ,H_m ,N*3 , d_H , N*3 ); // b -> d_b
    stat = cublasSetMatrix (3 ,N , sizeof (* Y_m) ,Y_m ,3 , d_Y , 3); // c -> d_c
    stat = cublasSetMatrix (N ,1 , sizeof (* YY) ,YY ,N , d_YY , N); // c -> d_c

    //stat= cublasSetVector(3, sizeof(*Y_m),Y_m, 3, d_Y, 3);
    float al =1.0f ;    // al =1
    float bet =1.0f ;    // bet =1
    float result ;
    //float const *const tmpY[] = {d_Y, 0 };
    //float *const tmpYY[] = {d_YY, 0 };
    cublasSetPointerMode(handle,CUBLAS_POINTER_MODE_DEVICE);
    //stat=cublasSnrm2(handle,N,d_Y,3,d_YY);
    //stat=cublasSdot(handle,3,d_Y,3,d_Y,3,d_YY);
    std::cout << "Before-Dot"<<"\n";
    Dot(d_Y,N,3,d_YY);
    std::cout << "After-Dot"<<"\n";
    stat = cublasGetMatrix (N ,1 , sizeof (* YY ) , d_YY ,N ,YY, N ); // cp d_c - > c
    std::cout << YY[0] <<"\n";
    std::cout << YY[1] <<"\n";
    int size = N * sizeof(float);
    cudaMemcpy(array_host_, d_YY, size, cudaMemcpyDeviceToHost);

}
*/

void cuda_SP::Kmeans(float* array_host_,float* XData,float* H_C,float* Y_C,float* r_ik,float* pi,float* sigma1,float* sigma2,int* ind,int* Lookup)
//void cuda_SP::Kmeans(int* array_host_,float* XData,float* H_C,float* Y_C,float* r_ik,float* pi,float* sigma1,float* sigma2,int* ind,int* Lookup)

{

    float* d_X ;    // d_a - a on the device
    float* d_X_t;    // d_a - a on the device

    float* d_H ;    // d_b - b on the device
    float* d_H_t ;    // d_b - b on the device

    float* d_Y ;    // d_c - c on the device
    float* d_r_ik;
    float* d_r_ik_t;
    float* d_pi;
    float* d_sigma1;
    float* d_sigma2;

    float* d_YY ;    // d_c - c on the device0
    float* d_HH ;    // d_c - c on the device
    float* d_T1 ;
    float* d_T2 ;
    float* d_T3 ;
    float* d_C1;
    float* d_C2;
    float* d_C1_T;
    float* d_C;


    float* d_T1_temp;
    float* d_m_prime_i;
    float* d_eta_prime;
    float* d_n_k;
    float* d_XC1;
    float* d_HC2;
    float* d_HC2_t;

    float* d_distances1;
    float* d_distances2;

    float* d_NK;

    int* d_PaddedAmax;

    int* d_idx_pixels;

    int* d_matrices;

    int* d_numbers;

    int* d_Lookup;

    float* d_rik_max;
    float* d_rik_copy;


    cublasStatus_t stat ;
    // CUBLAS functions status
    cublasHandle_t handle ;

    stat = cublasCreate (& handle ); // initialize CUBLAS context

    gpuErrchk(cudaMalloc (( void **)& d_X , N * 5 * sizeof (dtype ))); // device    // memory alloc for d_X
    gpuErrchk(cudaMalloc (( void **)& d_X_t , N * 5 * sizeof (dtype ))); // device    // memory alloc for d_X
    gpuErrchk(cudaMalloc (( void **)& d_H , N * 3 * 2 * sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_H_t , N * 3 * 2 * sizeof (dtype))); // device    // memory alloc for d_H

    gpuErrchk(cudaMalloc (( void **)& d_Y , N * 3 * sizeof (dtype ))); // device    // memory alloc for d_y
    gpuErrchk(cudaMalloc (( void **)& d_r_ik , N * K * sizeof (dtype ))); // device    // memory alloc for d_y
    gpuErrchk(cudaMalloc (( void **)& d_rik_max , N * K * sizeof (dtype ))); // device    // memory alloc for d_y

    gpuErrchk(cudaMalloc (( void **)& d_r_ik_t , N * K * sizeof (dtype ))); // device    // memory alloc for d_y
    gpuErrchk(cudaMalloc (( void **)& d_rik_copy , N * K * sizeof (dtype ))); // device    // memory alloc for d_y
    gpuErrchk(cudaMalloc (( void **)& d_pi , K * 1 * sizeof (dtype ))); // device    // memory alloc for d_y

    gpuErrchk(cudaMalloc (( void **)& d_sigma1 , 7 * 1 * sizeof (dtype ))); // device    // memory alloc for d_y
    gpuErrchk(cudaMalloc (( void **)& d_sigma2 , 3 * 1 * sizeof (dtype ))); // device    // memory alloc for d_y

    gpuErrchk(cudaMalloc (( void **)& d_YY ,N * 1 * sizeof (dtype))); // device    // memory alloc for d_YY
    gpuErrchk(cudaMalloc (( void **)& d_HH ,N * 4 * sizeof (dtype))); // device    // memory alloc for d_HH

    gpuErrchk(cudaMalloc (( void **)& d_T1 ,K * 2 * sizeof (dtype))); // device    // memory alloc for d_HH
    gpuErrchk(cudaMalloc (( void **)& d_T2 ,K * 4 * sizeof (dtype))); // device    // memory alloc for d_HH
    gpuErrchk(cudaMalloc (( void **)& d_T3 ,K * 1 * sizeof (dtype))); // device    // memory alloc for d_HH

    gpuErrchk(cudaMalloc (( void **)& d_C1 ,K * 5 * sizeof (dtype))); // device    // memory alloc for d_HH
    gpuErrchk(cudaMalloc (( void **)& d_C2 ,K * 2 * sizeof (dtype))); // device    // memory alloc for d_HH
    gpuErrchk(cudaMalloc (( void **)& d_C1_T ,K * 5 * sizeof (dtype))); // device    // memory alloc for d_HH

    gpuErrchk(cudaMalloc (( void **)& d_C ,K * 7 * sizeof (dtype))); // device    // memory alloc for d_HH

    gpuErrchk(cudaMalloc (( void **)& d_n_k ,K * sizeof (dtype))); // device    // buffer for n_kk
    gpuErrchk(cudaMalloc (( void **)& d_T1_temp ,N * 2 * sizeof (dtype))); // device    // memory alloc for d_HH
    gpuErrchk(cudaMalloc (( void **)& d_m_prime_i ,K *4* sizeof (dtype))); // device    // buffer for n_kk
    gpuErrchk(cudaMalloc (( void **)& d_eta_prime ,K *2* sizeof (dtype))); // device    // buffer for n_kk
    gpuErrchk(cudaMalloc (( void **)& d_HC2 , K * N * 3 * sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_XC1 , K * N * 5 * sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_distances1 , K * N * sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_distances2 , K * N  * sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_NK , K * sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_PaddedAmax , 642*482 * sizeof (int))); // device    // memory alloc for d_H

    gpuErrchk(cudaMalloc (( void **)& d_matrices , int(N/4)*81* sizeof (dtype))); // device    // memory alloc for d_H
    gpuErrchk(cudaMalloc (( void **)& d_numbers , int(N/4)*9* sizeof (int))); // device    // memory alloc for d_H

    gpuErrchk(cudaMalloc (( void **)& d_HC2_t , K * N  * 3* sizeof (dtype))); // device    // memory alloc for d_H


    gpuErrchk(cudaMalloc (( void **)& d_idx_pixels , int(N /4) *4* 9 * sizeof (int))); // device    // memory alloc for d_H

    gpuErrchk(cudaMalloc (( void **)& d_Lookup , 256 * sizeof (int))); // device    // memory alloc for d_H


    gpuErrchk(cudaMemcpy(d_X, XData, N*5* sizeof(dtype), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_H, H_C, N*6*sizeof(dtype), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_Y, Y_C, N*3* sizeof(dtype), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_r_ik, r_ik, N*K* sizeof(dtype), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_sigma1, sigma1, 7* sizeof(dtype), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_sigma2, sigma2, 3* sizeof(dtype), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_idx_pixels, ind, int(N /4) *4* 9 *sizeof(int), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_Lookup, Lookup, 256 *sizeof(int), cudaMemcpyHostToDevice));


    //cublasSetPointerMode(handle,CUBLAS_POINTER_MODE_DEVICE);


    BmmHTH(d_H,d_HH);
    BmmHTYT(d_H,d_Y,d_T1_temp);
    //stat=cublasSgeam(handle,CUBLAS_OP_T,CUBLAS_OP_T,K,N,&al,d_r_ik,N,&
    float al =1.0f ;    // al =1
    float bet =0.0f ;    // bet =1
    int i=0;
    int mod;
    stat=cublasSgeam( handle, CUBLAS_OP_T, CUBLAS_OP_N, N*3, 2, &al, d_H, 2, &bet, d_r_ik, N*3, d_H_t, N*3 ); //transpose r_ik
    stat=cublasSgeam( handle, CUBLAS_OP_T, CUBLAS_OP_N, N, K, &al, d_r_ik, K, &bet, d_r_ik, N, d_r_ik_t, N ); //transpose r_ik





    for(i=0;i<900;i++)
    {
        mod=i%4;
        std::cout << i<<"\n";
        gpuErrchk(cudaMemcpy(d_rik_copy, d_r_ik, N*K *sizeof(dtype), cudaMemcpyDeviceToDevice));
        Mstep_SP(handle,d_X,d_r_ik_t,d_n_k,d_C1);
        Mstep_SP_F(handle,d_r_ik_t,d_HH,d_YY,d_T1_temp,d_T1,d_T2,d_T3,d_n_k,d_m_prime_i,d_C2,d_pi); //d_n_k as buffer
        Estep_SP(handle,d_X,d_H_t,d_Y,d_r_ik,d_r_ik_t,d_pi,d_NK,d_C1,d_C2,d_sigma1,d_sigma2,d_XC1,d_HC2,d_distances1,d_distances2,d_HC2_t);
        Connectivity(d_rik_copy,d_rik_max,d_r_ik,d_PaddedAmax,d_matrices,d_numbers,d_idx_pixels,mod,d_Lookup);
        gpuErrchk(cudaMemcpy(d_r_ik, d_rik_max, N*K *sizeof(dtype), cudaMemcpyDeviceToDevice));
        stat=cublasSgeam( handle, CUBLAS_OP_T, CUBLAS_OP_N, N, K, &al, d_r_ik, K, &bet, d_r_ik, N, d_r_ik_t, N ); //transpose r_ik

    }

    gpuErrchk(cudaMemcpy(array_host_, d_r_ik, N*K*sizeof(dtype), cudaMemcpyDeviceToHost));

    //stat = cublasGetMatrix (N ,K , sizeof (dtype ) , d_r_ik_t ,N ,array_host_, N ); // cp d_c - > c
    //gpuErrchk(cudaMemcpy(array_host_, d_HH, N*K* sizeof(dtype), cudaMemcpyDeviceToHost));


}

cuda_SP::~cuda_SP() {
  cudaFree(array_device);
}
