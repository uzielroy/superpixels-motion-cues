import numpy as np
cimport numpy as np

assert sizeof(int) == sizeof(np.int32_t)

cdef extern from "src/manager.hh":
    cdef cppclass C_cuda_SP "cuda_SP":
        C_cuda_SP(np.int32_t*, int)
        void increment()
        void retreive()
        void retreive_to(np.int32_t*, int)
        void KmeansC2(float*,float*,float*,float*,float*,float*,float*,int*,int*)
        void Kmeans(np.float32_t*,float*,float*,float*,float*,float*,float*,float*,int*,int*)
        #void Kmeans(np.int32_t*,float*,float*,float*,float*,float*,float*,float*,int*,int*)


cdef class cuda_SP:
    cdef C_cuda_SP* g
    cdef int dim1

    def __cinit__(self, np.ndarray[ndim=1, dtype=np.int32_t] arr):
        self.dim1 = len(arr)
        self.g = new C_cuda_SP(&arr[0], self.dim1)
        print("cinit")
    def increment(self):
        self.g.increment()

    def retreive_inplace(self):
        self.g.retreive()

    def retreive(self):
        cdef np.ndarray[ndim=1, dtype=np.int32_t] a = np.zeros(self.dim1, dtype=np.int32)
        self.g.retreive_to(&a[0], self.dim1)
        return a


    def KmeansC2(self,np.ndarray[float,ndim=1,mode="c"]XData not None,
                 np.ndarray[float,ndim=1,mode="c"] H not None,
                 np.ndarray[float,ndim=1,mode="c"]Y not None,
                 np.ndarray[float,ndim=1,mode="c"]r_ik not None,
                 np.ndarray[float,ndim=1,mode="c"]pi not None,
                 np.ndarray[float,ndim=1,mode="c"]sigma1 not None,
                 np.ndarray[float,ndim=1,mode="c"]sigma2 not None,
                 np.ndarray[int,ndim=1,mode="c"]ind not None,
                 np.ndarray[int,ndim=1,mode="c"]lookup not None,

                 ):
        print("KmeansC2")
        cdef float[::1] XDataC = XData
        cdef float[::1] HC = H
        cdef float[::1] YC = Y
        cdef float[::1] r_ikC = r_ik
        cdef float[::1] piC = pi
        cdef float[::1] sigma1C = sigma1
        cdef float[::1] sigma2C = sigma2
        cdef int[::1] indC = ind
        cdef int[::1] LookupC = lookup

        cdef np.ndarray[ndim=1, dtype=np.float32_t] a = np.zeros(int(480*640*17), dtype=np.float32)
        #cdef np.ndarray[ndim=1, dtype=np.int32_t] a = np.zeros(int(482*642), dtype=np.int32)

        self.g.Kmeans(&a[0],&XDataC[0],&HC[0],&YC[0],&r_ikC[0],&piC[0],&sigma1C[0],&sigma2C[0],&indC[0],&LookupC[0])
        return a
