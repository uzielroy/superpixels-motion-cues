#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 14:45:03 2018

@author: uzielr
"""


"""
************************************************************************************************************************************************************

                                                                            Imports

************************************************************************************************************************************************************
"""

from AngleImpl import *


import Sample_Functions as my_sample
import Help_Functions as my_help
from PIL import Image, ImageEnhance


from scipy import ndimage as ndi

from mpl_toolkits import mplot3d
from colorama import Fore, Back, Style
import math
from scipy import signal
import scipy.stats 
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
from matplotlib.patches import Ellipse
import cv2
from numpy.linalg import inv
import warnings
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import cm
from copy import deepcopy
from statistics import median

warnings.filterwarnings("ignore")


from IPython.terminal.embed import InteractiveShellEmbed                                        
ip = InteractiveShellEmbed( banner1 = 'Dropping into IPython',
                                     exit_msg = 'Leaving IPython, back to program.')
 


"""
************************************************************************************************************************************************************

                                                                        Plot Funtions

************************************************************************************************************************************************************
"""




def Plotlikelihood(T1,T2,T3):
    
    """Plotting the liklihood function, can used by :meth:`AngleImpl.EstimateParams`
    
    **Parameters**:
     - :math:`T1` - T1 matrix
     
     - :math:`T2` - T2 matrix
     
     - :math:`T3` - T3 matrix
             
    **Returns**:
     - Plot of the liklihood function

    .. note:: Randomize the probability uniformly 

    .. todo:: Make the initialzation similar for adjacent pixels
    
    """
    
    
    res=101
    x_likelihood = np.linspace(-6, 6, res)
    y_likelihood = np.linspace(-6, 6, res)
    X_likelihood, Y_likeilhood = np.meshgrid(x_likelihood, y_likelihood)
    Z_likelihood=T3+T2[0,0]*X_likelihood**2+T2[1,1]*Y_likeilhood**2+X_likelihood*Y_likeilhood*(T2[0,1]+T2[1,0])-2*T1[0]*X_likelihood-2*T1[1]*Y_likeilhood
    
    plt.subplot(3,2,5)
    
    minIndex=np.argmin(Z_likelihood.ravel())
    #plot points
    plt.imshow(Z_likelihood)
    plt.colorbar()
    plt.axis('off')
    Xmin=X_likelihood.ravel()[minIndex]
    Ymin=Y_likeilhood.ravel()[minIndex]    
    plt.plot(Xmin+res/2, Ymin+res/2,'rx')
    print(Back.RED)
    print("Min Point from the LL:  " , Xmin,Ymin)
    print(Style.RESET_ALL)
    return


def PlotPoints(P,color,label):
    
    """Plotting points(x,y) with given color and adding label to the plot
    
    **Parameters**:
     - :math:`P[Np,Dp]' Points [Number of points ,Dimension of points]
     
     - color- color of the points
     
     - label- label of the points
             
    **Returns**:
     - Plot of points with given color and the given label
    
    """
    plt.scatter(P[:,0],P[:,1],color=color,label=label,s=0.01)
    plt.legend(loc='upper left')
    plt.show()
    

def PlotContour(mu,sigma,X,color,label):
    
        
    """Plotting contour level of the gaussian distribuation
    
    **Parameters**:
     - :math:`\\mu` - Mean
     
     - :math:`\\Sigma` - Covariance 

     - :math:`X` - Points sampled from the gaussian
     
     - color- color of the contour
     
     - label- label of the contour
             
    **Returns**:
     - Plot of the contour with given color and the given label
    
    .. note:: scale is 0.65 from the max point
    """
    
    eig = np.linalg.eig(sigma)
    eig_val_max = np.argmax(eig[0])
    eig_vect = eig[1][eig_val_max]
    angle=math.degrees(math.atan(eig_vect[0]/eig_vect[1]))
    maxXY=np.amax([np.amax(X[0,:]),np.amax(X[1,:])])
    scale=0.65*maxXY
    ax = plt.gca()
    ellipse = Ellipse(xy=mu, width=scale*sigma[0,0], height=scale*sigma[1,1],angle=angle,edgecolor=color, fc='None', lw=10,label=label)
    ax.add_patch(ellipse)



def PlotLines(X,ProjPoints):
    
    """Plotting lines between 2 sets of points using :meth:`AngleImpl.newline`.
    
    **Parameters**:
     - P1 - Set of points number 1
     
     - P2 - Set of points number 2
     
    **Returns**:
     - Showing the plot all of the loines between the sets of points.
    
    """
    
    for i in range (0,N):
        newline(X[i],ProjPoints[i])
    plt.show()



def newline(p1, p2):
    
    """Plotting lines between 2 points . Called by :meth:`AngleImpl.PlotLines`.
    
    **Parameters**:
     - P1 - Point number 1
     
     - P2 - Point number 2
     
    **Returns**:
     - Add the line to the plot
    
    """
    ax = plt.gca() 
    xmin, xmax = ax.get_xbound()    

    if(p2[0] == p1[0]):
        xmin = xmax = p1[0]
        ymin, ymax = ax.get_ybound()
    else:
        ymax = p1[1]+(p2[1]-p1[1])/(p2[0]-p1[0])*(xmax-p1[0])
        ymin = p1[1]+(p2[1]-p1[1])/(p2[0]-p1[0])*(xmin-p1[0])

    l = mlines.Line2D([xmin,xmax], [ymin,ymax])
    ax.add_line(l)
    return l

