import time
from AngleImpl import *

import Sample_Functions as my_sample
import Plot_Functions as my_plot
from PIL import Image, ImageEnhance

from scipy import ndimage as ndi

from mpl_toolkits import mplot3d
from colorama import Fore, Back, Style
import math
from scipy import signal
import scipy.stats
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
from matplotlib.patches import Ellipse
import cv2
from numpy.linalg import inv
import warnings
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import cm
from copy import deepcopy
from statistics import median

warnings.filterwarnings("ignore")

from IPython.terminal.embed import InteractiveShellEmbed

ip = InteractiveShellEmbed(banner1='Dropping into IPython',
                           exit_msg='Leaving IPython, back to program.')

def Create_LookupTable():
    lookup = np.zeros(256)
    for i in range (0,256):
        bin=i
        matrix=np.zeros(9)
        for j in range(0,8):
            if(j>=4):
                matrix[j+1]= bin&1
            else:
                matrix[j] = bin & 1
            bin=bin>>1
        matrix=matrix.reshape((3,3))
        matrix=np.uint8(matrix)
        _, num0 = cv2.connectedComponents(matrix,connectivity=4)
        num0=np.amax(num0)
        matrix[1,1]=1
        _, num1 = cv2.connectedComponents(matrix,connectivity=4)
        num1=np.amax(num1)
        matrix=1-matrix
        _, num0_flip = cv2.connectedComponents(matrix,connectivity=4)
        num0_flip=np.amax(num0_flip)
        matrix[1,1]=1
        _, num1_flip = cv2.connectedComponents(matrix,connectivity=4)
        num1_flip=np.amax(num1_flip)
        if((num0_flip==num1_flip) and (num0==num1)):
                lookup[i]=1
        else:
                lookup[i]=0
    return lookup




def binary(num, length=8):
    return int(format(num, '#0{}b'.format(length + 2)))

def Create_Matrix(padded_matrix,mod):
    pixels=padded_matrix.index_select(0,Global.idx_pixels[mod]).view(9,-1)
    matrix=torch.add(-pixels.unsqueeze(0),pixels.unsqueeze(1))
    matrix=torch.add((matrix==0),(pixels==(-1)))
    return matrix>0

def Create_Matrix_Sub(padded_matrix_split,mod):
    pixels_sub=padded_matrix_split.index_select(0,Global.idx_pixels[mod]).view(9,-1)
    matrix_sub=torch.add(-pixels_sub.unsqueeze(0),pixels_sub.unsqueeze(1))
    matrix=((matrix_sub==0))
    return matrix>0





def Create_number(matrix):
    temp=torch.index_select(matrix,1,Global.shift_index)
    return (torch.sum(temp<<Global.shift,dim=1))

def Change_pixel(prev_r_ik,r_ik,index2,r_ik_t5,mod,c_vals,r_ik_tk,range_conn,split_prev_r_ik,c_vals_s,r_ik_s):

    padding = torch.nn.ConstantPad2d((1, 1, 1, 1), (-1))
    padded_matrix = padding(prev_r_ik)
    padded_matrix = padded_matrix.view(-1)
    number = Create_number(Create_Matrix(padded_matrix, mod))
    number = torch.index_select(number, 0, Global.ne4_idx)
    number2 = torch.index_select(Global.LOOKUP_TABLE, 0, number.view(-1))
    ind = torch.all(number2.view(5, -1), dim=0).float()  # ind that we can change
    index2.zero_()
    index2 = index2.scatter_(0, Global.idx[:, mod], ind)  # 0 -same as before , 1 - Can change (All N pixels)
    max_neigh=torch.argmax(r_ik, dim=1)
    valsNew = torch.add(max_neigh,range_conn)
    valsK = torch.where(index2 == 1, c_vals[valsNew], prev_r_ik.view(-1))
    split_prev_r_ik=torch.where(index2==1,c_vals_s[valsNew],split_prev_r_ik)

    padded_matrix_split = padding(split_prev_r_ik.view(Global.HEIGHT,-1))
    padded_matrix_split = padded_matrix_split.view(-1)
    number = Create_number(Create_Matrix_Sub(padded_matrix_split,mod))
    number = torch.index_select(number, 0, Global.ne4_idx)
    number2 = torch.index_select(Global.LOOKUP_TABLE, 0, number.view(-1))
    ind = torch.all(number2.view(5, -1), dim=0).float()  # ind that we can change
    index2.zero_()
    index2 = index2.scatter_(0, Global.idx[:, mod], ind)  # 0 -same as before , 1 - Can change (All N pixels)
    max_neigh = torch.argmax(r_ik_s, dim=1)
    valsNew = torch.add(max_neigh, range_conn)
    valsK2 = torch.where((torch.add(index2 == 1,c_vals[valsNew]==valsK))>1, c_vals_s[valsNew], split_prev_r_ik.view(-1))



    return valsK,valsK2

def Split(prev_r_ik,split_prev_r_ik,c1,c1_idx,idx_rand,clusters_LR):
    idx=idx_rand.long()
    #size_idx=idx.shape[0]
    padding = torch.nn.ConstantPad2d((1, 1, 1, 1), 0).cuda()
    padded_matrix = padding(split_prev_r_ik).reshape(-1).cuda()
    distances=torch.zeros(2,((Global.HEIGHT+2)*(Global.WIDTH+2))).float().cuda().transpose(0,1)
    centers=torch.index_select(c1,0,idx)[:,0:2]
    new_center= torch.add(centers, -2).floor_().int()
    new_center[:, 0]=new_center[:,0]+2
    new_center_idx=new_center[:,0]*(Global.WIDTH+2)+new_center[:,1]+1 #idx in padded
    new_center_idx=torch.masked_select(new_center_idx,((padded_matrix[new_center_idx.long()]==idx)+(new_center_idx>0))==2)
    prev_centers=padded_matrix.take(new_center_idx.long())
    if(new_center_idx.size()==0):
        return 0
    neigh=torch.from_numpy(np.array([-1,1,-(Global.WIDTH+2),(Global.WIDTH+2)])).cuda().int()
    data=torch.zeros((Global.HEIGHT+2)*(Global.WIDTH+2),2).cuda().int()
    data[:,0]=torch.arange(0,(Global.HEIGHT+2)*(Global.WIDTH+2)).int()
    data2=data.clone()
    data[new_center_idx.long(),1] = prev_centers.int()
    loop=1
    distance_add=Global.ones.clone()


    valid_v = new_center_idx
    while (loop):
        distance_add.add_(1)
        exp_clusters = torch.take(padded_matrix, valid_v.long()).unsqueeze(1).repeat(1, 4).reshape(-1)
        idx = torch.add(neigh.unsqueeze(0), valid_v.unsqueeze(1))
        padded_matrix.index_fill_(0, valid_v.long(), 0)
        valid_ind = torch.masked_select(idx.reshape(-1), padded_matrix[idx.reshape(-1).long()] == exp_clusters)
        data[valid_ind.long(), 1] = padded_matrix[valid_ind.long()].int()
        valid_ind = torch.masked_select(data[:, 0], data[:, 1] > 0)
        if (valid_ind.shape[0] < 1):
            loop = 0
        else:
            distances[:, 0].index_add_(0, valid_ind.long(), distance_add[0:valid_ind.shape[0]])
            data[:, 1].zero_()
            valid_v = valid_ind

    distances[new_center_idx.long(),0]=0
    padded_matrix = padding(split_prev_r_ik).reshape(-1).cuda()
    #idx=torch.arange(1,Global.K_C).cuda()
    idx=idx_rand.long()
    centers = torch.index_select(c1, 0, idx)[:, 0:2]
    new_center = torch.add(centers, +2).ceil_().int()
    new_center[:,0]=new_center[:,0]-2
    new_center_idx = new_center[:, 0] * (Global.WIDTH + 2) + new_center[:, 1] + 1  # idx in padded
    new_center_idx=torch.masked_select(new_center_idx,padded_matrix[new_center_idx.long()]==idx)
    prev_centers = padded_matrix.take(new_center_idx.long())
    # TODO: need to check that new centers are valid!!
    neigh = torch.from_numpy(np.array([-1, 1, -(Global.WIDTH + 2), (Global.WIDTH + 2)])).cuda().int()
    data[:,1].zero_()
    data[new_center_idx.long(), 1] = prev_centers.int()
    loop = 1
    distance_add=Global.ones.clone()
    valid_v=new_center_idx
    while (loop):
        distance_add.add_(1)
        exp_clusters = torch.take(padded_matrix, valid_v.long()).unsqueeze(1).repeat(1, 4).reshape(-1)
        idx = torch.add(neigh.unsqueeze(0), valid_v.unsqueeze(1))
        padded_matrix.index_fill_(0, valid_v.long(), 0)
        valid_ind=torch.masked_select(idx.reshape(-1),padded_matrix[idx.reshape(-1).long()] == exp_clusters)
        data[valid_ind.long(), 1] = padded_matrix[valid_ind.long()].int()
        valid_ind = torch.masked_select(data[:, 0], data[:, 1] > 0)

        if (valid_ind.shape[0] < 1):
             loop=0
        else:
            distances[:, 1].index_add_(0, valid_ind.long(), distance_add[0:valid_ind.shape[0]])
            data[:, 1].zero_()
            valid_v=valid_ind


    #idx=torch.arange(1,Global.K_C).cuda()
    idx=idx_rand.long()
    idx_args=torch.arange(0,idx.shape[0]).cuda()
    split_idx=torch.masked_select(data[:,0],distances[:,1]>0).long()
    idx_final=torch.zeros(idx.shape[0],2).cuda()
    idx_final[:,0]=idx_args
    idx_final[:,0]=idx
    padded_matrix = padding(split_prev_r_ik).reshape(-1).cuda()
    distances[new_center_idx.long(),1]=0
    new_idx=torch.where(distances[split_idx,0]<distances[split_idx,1],Global.zeros[:distances[split_idx,0].shape[0]],Global.ones[:distances[split_idx,0].shape[0]])
    new_cluster_idx=torch.masked_select(split_idx,new_idx.byte())
    #padded_matrix[split_idx]=new_idx.long()
    data[:,1].zero_()
    idx=idx.long()
    #if(idx.shape[0]==Global.K_C):
    data2[idx,1]=Global.N_index[0:idx.shape[0]].int()+2+(Global.K_C)
    # else:
    #     data2[idx, 1] = Global.N_index[0:idx.shape[0]].int()+(Global.K_C*2)
    data[new_cluster_idx.long(),1]= (padding(split_prev_r_ik).reshape(-1).cuda()[new_cluster_idx]).int()
    data[new_cluster_idx.long(),1]=(data2[(padding(split_prev_r_ik).reshape(-1).cuda()[new_cluster_idx]).long(),1])
    # padded_matrix.add_(data[:,1].long())
    padded_matrix[new_cluster_idx.long()]=(data[new_cluster_idx.long(),1].long()).clone()
    clusters_LR[idx,1]=data2[idx,1]

    # prev_r_ik = padding(prev_r_ik).reshape(-1).cuda()
    # prev_r_ik[split_idx.long()]=(padding(split_prev_r_ik.cuda()).reshape(-1)[split_idx.long()]).clone()
    padded_matrix=padded_matrix.reshape(Global.HEIGHT+2,-1)
    #prev_r_ik=prev_r_ik.reshape(Global.HEIGHT+2,-1)
    return padded_matrix[1:Global.HEIGHT+1,1:Global.WIDTH+1].reshape(-1),new_idx.reshape(-1),clusters_LR