#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 14:45:03 2018

@author: uzielr
"""


"""
************************************************************************************************************************************************************

                                                                            Imports

************************************************************************************************************************************************************
"""
import Global
from AngleImpl import *

import Help_Functions as my_help
import Plot_Functions as my_plot
from PIL import Image, ImageEnhance


from scipy import ndimage as ndi

from mpl_toolkits import mplot3d
from colorama import Fore, Back, Style
import math
from scipy import signal
import scipy.stats 
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
from matplotlib.patches import Ellipse
import cv2
from numpy.linalg import inv
import warnings
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import cm
from copy import deepcopy
from statistics import median

warnings.filterwarnings("ignore")


from IPython.terminal.embed import InteractiveShellEmbed                                        
ip = InteractiveShellEmbed( banner1 = 'Dropping into IPython',
                                     exit_msg = 'Leaving IPython, back to program.')
 

"""
************************************************************************************************************************************************************

                                                                        Sample Funtions

************************************************************************************************************************************************************
"""
   

def SampleXK():
    
    """Samples Data points , used by the :meth:`AngleImpl.ArttificalFlowK`.
    
    **Parameters**:
     - 

    **Returns**:
     - :math:`Xk[K,N\K]` - Mean [Number of clusters + outlayer,Dimenstion of data] . 
     
     - :math:`X[N,D]` - Covariance  [Number of clusters + outlayer,Dimenstion of data,Dimenstion od data] . 

     - :math:`U[HEIGHT,WIDTH]` . optical flow X vector
     
     - :math:`V[HEIGHT,WIDTH]` . optical flow Y vector
     
     - :math:`\\mu [K,D]` - Mean [Number of clusters,Dimenstion of data] . 
     
     - :math:`\\Sigma [K,D,D]` - Covariance  [Number of clusters ,Dimenstion of data,Dimenstion od data] . 

     - :math:`C [K,D]` - C [Number of clusters + outlayer,Dimenstion of data] . 
     
     - :math: FramePoints[HEIGHT,WIDTH,3] - Frame with diffrent color for each pixel. 

    """
    mu=np.zeros((Global.K,Global.D))
    sigma=np.zeros((Global.K,Global.D,Global.D))
    indx=np.arange(Global.N).reshape((Global.HEIGHT,Global.WIDTH))
    X=np.zeros((Global.N,Global.D))
    Xk=[]
    mu[:,0]=np.random.uniform(-2.5,2.5,Global.K)
    mu[:,1]=np.random.uniform(-2.5,2.5,Global.K)
    c=np.random.uniform(8,9,Global.K)
    sigma=np.asarray([(np.eye(Global.D)*1.0/c[i]) for i in range(Global.K) ])
    framePoints=np.zeros((Global.N,3))
    kH=int(Global.HEIGHT/2)
    kW=int(Global.WIDTH/2)
    Xk=np.zeros((Global.K,kH*kW,Global.D))
    indx=my_help.blockshaped(indx,kH,kW)


    for i in range(0,Global.K):
        Xk[i]=SampleX(mu[i],sigma[i],kH*kW)
        X[indx[i].reshape(kH*kW)]=Xk[i]
        framePoints[indx[i].reshape(kH*kW)]=my_help.hex2rgb(Global.colors[i+1])

    U=X.reshape((Global.HEIGHT,Global.WIDTH,2))[:,:,0]
    V=X.reshape((Global.HEIGHT,Global.WIDTH,2))[:,:,1]
    U=U.astype(np.float32)
    V=V.astype(np.float32)
    
    return Xk,X,U,V,mu,sigma,c,framePoints.reshape((Global.HEIGHT,Global.WIDTH,3))
    

def SampleNG(alpha=0,beta=0,eta=0,m=0):
    
        
    """Samples from normal gamma distribution , used for estimating the parameters :meth:`AngleImpl.EstimateParams`.

    **Parameters**:
     - :math:`\\alpha` - shape parameter
     
     - :math:`\\beta` - rate parameter
     
     - :math:`\\eta` - mean of nomral distribution
     
     - :math:`M` - used for sampling the covariance

    **Returns**:
     - :math:`\\mu_{max}` - Mean 
     
     - :math:`\\Sigma_{max}` - Covariance

     - :math:`C_{max}` - C 
     
     .. note:: The function only computes the mode of the normal-gamma ditributin. Sampling is in comment.
    
    """

    #c=sp.stats.gamma.rvs(alpha,1,scale=(1.0/beta)) 
    #sigma=inv(c*m)
    #mu=sp.random.multivariate_normal(eta[:,0],sigma,1)
    mu_max=eta[:,:,0]
    c_max=(alpha-0.5)/(beta)
    #sigma_sample=inv(c*np.eye(D))

    #sigma_max=inv(c_max[:,:,np.newaxis]*np.eye(Global.D)[np.newaxis]) # not torch
    eye_D=torch.eye(Global.D)
    sigma_max=my_help.apply(torch.inverse,c_max[:,:,np.newaxis]*eye_D)

    return mu_max,sigma_max,c_max


def SampleNGTF(alpha=0, beta=0, eta=0, m=0):
    """Samples from normal gamma distribution , used for estimating the parameters :meth:`AngleImpl.EstimateParams`.

    **Parameters**:
     - :math:`\\alpha` - shape parameter

     - :math:`\\beta` - rate parameter

     - :math:`\\eta` - mean of nomral distribution

     - :math:`M` - used for sampling the covariance

    **Returns**:
     - :math:`\\mu_{max}` - Mean

     - :math:`\\Sigma_{max}` - Covariance

     - :math:`C_{max}` - C

     .. note:: The function only computes the mode of the normal-gamma ditributin. Sampling is in comment.

    """

    mu_max = eta
    c_max = torch.div((alpha - 0.5) , (beta))
    eye_D = torch.eye(Global.D).float().cuda(async=True)
    #sigma_max = my_help.apply(torch.inverse, c_max.unsqueeze_(2) * eye_D)

    return mu_max, eye_D, c_max


def SampleX(mu,sigma,n=0):
    X=sp.random.multivariate_normal(mu,sigma,int(n))
    return X
        
def sampleTheta():
   Theta= (sp.random.uniform(0,Global.PI,(Global.N,1)))
   return Theta
